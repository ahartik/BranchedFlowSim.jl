module BranchedFlowSim

include("colorschemes.jl")
include("quantum_potentials.jl")
include("potentials.jl")
include("potential_argparse.jl")
include("quasi2d.jl")
include("classical.jl")
include("helpers.jl")
include("quantum.jl")


end # module BranchedFlowSim
