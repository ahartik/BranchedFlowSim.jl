using BranchedFlowSim
using CairoMakie
using Makie
using LinearAlgebra

# Simulation parameters
L = 20
num_grid_points = 1024
# Potential parameters
correlation_scale = 0.5
V0 = 50
# Initial wavefunction parameters
packet_pos = [-3, -4]
packet_momentum = [150, 0]
E_kin = norm(packet_momentum).^2/2
packet_width = 0.5
# Output parameters
eigenfunction_E = [E_kin]
path_prefix = "outputs/qm_nice"
# colorscheme = BranchedFlowSim.RealPartColor(BranchedFlowSim.sigmoid_berlin)
colorscheme = BranchedFlowSim.ComplexHSV()

dt = 1 / (2*E_kin)
# dt = 0.002
T = 8 * L / norm(packet_momentum)
num_steps = round(Int, T/dt)

mkpath(dirname(path_prefix))

xgrid = LinRange(-L / 2, L / 2, num_grid_points)
# potential = V0 * gaussian_correlated_random(xgrid, xgrid, correlation_scale) +
#             absorbing_potential(xgrid, 50, 1)
r2 = xgrid.^2 .+ transpose(xgrid.^2)

pow = 12
potential = (10 * E_kin / (xgrid[1]^pow) ) * r2.^(pow / 2)
potential = min.(potential, 10 * E_kin)
Ψ = gaussian_packet(xgrid, packet_pos, packet_momentum, packet_width)

evolution = time_evolution(xgrid, xgrid, potential, Ψ, dt, num_steps)

Ψs = nothing # Just to free memory
@time "video done" make_animation(path_prefix * ".mp4", evolution, potential;
    max_modulus=0.5, duration=20, colorscheme=colorscheme)

# Also compute an eigenfunction for some energy.

ΨE = collect_eigenfunctions(evolution, eigenfunction_E)
save(path_prefix * "_eigenfunc.png",
    wavefunction_to_image(
        ΨE[:, :, 1],
        colorscheme=colorscheme
    ))


# Make a shorter video
eigenΨ = ΨE[:,:,1]
eigen_T = 3 * 2π / eigenfunction_E[1]
eigen_num_steps = round(Int, eigen_T / dt)
eigen_evolution = time_evolution(xgrid, xgrid, potential, eigenΨ, dt, eigen_num_steps)
# Evolution 
@time "video done" make_animation(path_prefix * "_eigen.mp4", eigen_evolution, potential; colorscheme=colorscheme,
    max_modulus=0.5, duration=1)
