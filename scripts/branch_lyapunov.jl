# Code for a plot combining branches with lyapunov exponent
using BranchedFlowSim
using Printf
using Colors
using HDF5
using ColorSchemes
using LinearAlgebra
using Makie
using ImageShow
using Statistics
using CairoMakie

function make_trajectory_histogram(trajectories, xs, ys)
    dx = xs[2] - xs[1]
    dy = ys[2] - ys[1]
    num_particles = size(trajectories)[3]
    hist = zeros(length(ys), length(xs))
    for j ∈ 1:num_particles
        lx::Float64 = -1
        ly::Float64 = -1
        px::Int = 0
        py::Int = 0
        for (x, y) ∈ eachcol(@view trajectories[:, :, j])
            xi = 1 + (x - xs[1]) / dx
            yi = 1 + (y - ys[1]) / dy
            if 1 < xi < Nh && 1 < yi < Nh && 1 < lx < Nh && 1 < ly < Nh
                # Loop through all cells touching the line.
                line_integer_cells(lx, ly, xi, yi) do x, y
                    if x != px || y != py
                        hist[y, x] += 1
                    end
                    px = x
                    py = y
                end
            end
            lx = xi
            ly = yi
        end
    end
    return hist
end

# pot = correlated_random_potential(width, height, 0.1, v0, 0)
# pot = fermi_lattice()
# 
dot_radius = 1/4
softness = 0.15
v0 = 0.15
offset = [0.5, 0.5]
pot = LatticePotential(I, dot_radius, v0, offset=offset, softness=softness)

num_rays = 10000
# angles = LinRange(-pi/4, 3pi/4, num_rays + 1)[1:end-1]
angles = LinRange(0, 2pi, num_rays + 1)[1:end-1]
r0::Vector{Float64} = [0.0012, 0.0023]
p0::Matrix{Float64} = hcat((normalized_momentum(pot, r0, [cos(θ), sin(θ)]) for θ ∈ angles)...)

# First compute lyapunov for each p0
lyapunov = zeros(num_rays)
# Some params
dt = 0.004
T = 1000
M = 1
@time "Lyapunov" begin
    Threads.@threads for i ∈ 1:num_rays
        # 
        lyapunov[i] = 
            mean([
                local_lyapunov_exponent(pot, r0, p0[:, i], dt, T)
                for _ ∈ 1:M
            ])
    end
end
lines(angles, lyapunov, axis=(title="dt=$(dt) T=$T M=$M",))
display(current_figure())

## Compute trajectories

T = 100
dt = 0.005
saveat = 0.02
trajectory_length = 1 + convert(Int, T / saveat)

trajectories::Array{Float64,3} = zeros(2, trajectory_length, num_rays)
Threads.@threads for j ∈ 1:num_rays
    ts, rs, ps = ray_trajectory(r0, p0[:, j], pot, T, dt, saveat)
    trajectories[:, :, j] = rs
end

## Write data out

basename = @sprintf("data_v0_%.2f_r_%.2f_softness_%.2f.h5",
        v0, dot_radius, softness)
h5_path = "outputs/lyapunov_branches/$basename"
h5open(h5_path, "w") do f
    f["dt"] = Float64(dt)
    f["T"] = Float64(T)
    f["v0"] = Float64(v0)
    f["dot_radius"] = Float64(dot_radius)
    f["num_rays"] = Int64(num_rays)
    f["angles"] = collect(angles)
    f["lyapunov"] = lyapunov
    f["trajectories"] = trajectories
end

## Plot with lyapunov colors

W = 10
Nh = 600
xs = LinRange(-W, W, Nh)
ys = LinRange(-W, W, Nh)
maxD =  10 * num_rays / 100

is_stable = lyapunov .< 0.1*maximum(lyapunov)
chaos_hist = make_trajectory_histogram(trajectories[:,:,.!is_stable], xs, ys)
super_hist = make_trajectory_histogram(trajectories[:,:,is_stable], xs, ys)
img = begin
    pot_colormap = ColorSchemes.Greys
    colors_super = ColorScheme([
        RGB(1, 1, 1),
        RGB(0, 0, 1)
    ])
    colors_chaos = ColorScheme([
        RGB(1, 1, 1),
        RGB(1, 0, 0)
    ])

    V = grid_eval(xs, ys, pot)
    minV, maxV = extrema(V)
    minD = 0.0
    maxLD = Makie.pseudolog10(maxD)
    pot_img = get(pot_colormap, 0.4 * (V .- minV) ./ (maxV - minV + 1e-9))
    chaos_img = get(colors_chaos, Makie.pseudolog10.(chaos_hist) / maxLD)
    super_img = get(colors_super, Makie.pseudolog10.(super_hist) / maxLD)
    mapc.((s,c,v) -> min(s,c,v), super_img, chaos_img, pot_img)
end

basename = @sprintf("img_v0_%.2f_r_%.2f_softness_%.2f.png",
        v0, dot_radius, softness)
output_path = "outputs/lyapunov_branches/$(basename)"
save(output_path, reverse(img, dims=1))
print("Wrote $output_path")
# image(xs, xs, img, axis=(aspect=DataAspect(),))
display(img)