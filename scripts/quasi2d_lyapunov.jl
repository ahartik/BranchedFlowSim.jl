using BranchedFlowSim
using CairoMakie
using LaTeXStrings
using Printf
using Makie
using Colors
using LinearAlgebra
using ColorSchemes
using DifferentialEquations
using ChaosTools

function ray_f(dy::Float64, y::Float64, pot, t)
    return @inline force_y(pot, t, y)
end

struct Quasi2DIntegrator{Integrator<:OrdinaryDiffEq.ODEIntegrator}
    integrator::Integrator
    function Quasi2DIntegrator(pot::AbstractPotential,
        y::Real,
        py::Real,
        dt::Real)
        y0 = Float64(y)
        p0 = Float64(py)
        prob = SecondOrderODEProblem{false}(ray_f, p0, y0, (0, Inf), pot)
        method = Tsit5()
        # Yoshida6()
        integrator = init(prob, method, dt=dt, save_everystep=false)
        return new{typeof(integrator)}(
            integrator
        )
    end
end

function particle_y(qint::Quasi2DIntegrator)::Float64
    return qint.integrator.u[2]
end
function particle_py(qint::Quasi2DIntegrator)::Float64
    return qint.integrator.u[1]
end
function particle_t(qint::Quasi2DIntegrator)::Float64
    return qint.integrator.t
end
function particle_step!(qint::Quasi2DIntegrator)
    step!(qint.integrator)
    return nothing
end
function reset_particle!(qint::Quasi2DIntegrator, y, py)
    set_u!(qint.integrator,
        ArrayPartition((py, y))
    )
end

function poincare_intersections(pot, start_y, dt, end_T)
    py::Float64 = 0
    y::Float64 = start_y
    t = 0.0
    ints::Vector{SVector{6, Float64}} = []
    last_lcoords::SVector{2, Float64} = pot.A_inv * SVector(t, y)
    # last_lx = lcoords[1]
    # push!(ints, SVector(0, lcoords[2] - floor(lcoords[2])))
    while t < end_T
        py += dt * force_y(pot, t, y)
        y += dt * py
        t += dt
        # Map to lattice coords
        lcoords = pot.A_inv * SVector(t, y)
        # display(lcoords)
        if floor(Int, lcoords[1]) > floor(Int, last_lcoords[1])
            # Interpolate y
            x0 = last_lcoords[1] - floor(lcoords[1])
            x1 = lcoords[1] - floor(lcoords[1])
            @assert x0 <= 0 "x0=$x0 x1=$x1"
            # x0 < 0, x1 > 0
            # y = y0 + (-x0)*(y1)
            lx,ly = last_lcoords + (-x0/(x1-x0)) * (lcoords - last_lcoords)
            push!(ints, SVector(py, ly - floor(ly), lx, ly, t, y))
            last_lcoords = lcoords
        end
    end
    return ints
end

function lyapunov_inner(ref_particle, delta_particle, T)
    y1 = particle_y(ref_particle)
    py1 = particle_py(ref_particle)
    y2 = particle_y(delta_particle)
    py2 = particle_py(delta_particle)
    d2 = (y1 - y2)^2 + (py1 - py2)^2
    d_norm = sqrt(d2)
    d_min = d_norm / 10
    d_max = d_norm * 10
    d2_min = d_min^2
    d2_max = d_max^2
    log_sum::Float64 = 0.0
    while particle_t(ref_particle) < T
        particle_step!(ref_particle)
        particle_step!(delta_particle)
        # Possibly perform scaling

        y1 = particle_y(ref_particle)
        py1 = particle_py(ref_particle)
        y2 = particle_y(delta_particle)
        py2 = particle_py(delta_particle)
        d2 = (y1 - y2)^2 + (py1 - py2)^2
        if d2 < d2_min || d2 > d2_max
            dist = sqrt(d2)
            dist_scaling = d_norm / dist
            y2 = y1 + (y2 - y1) * dist_scaling
            py2 = py1 + (py2 - py1) * dist_scaling
            reset_particle!(delta_particle, y2, py2)
            log_sum -= log(dist_scaling)
        end
    end
    @assert(particle_t(ref_particle) ≈ particle_t(delta_particle),
        "$(particle_t(ref_particle)) != $(particle_t(delta_particle))")
    # Perform last scaling
    y1 = particle_y(ref_particle)
    py1 = particle_py(ref_particle)
    y2 = particle_y(delta_particle)
    py2 = particle_py(delta_particle)
    d2 = (y1 - y2)^2 + (py1 - py2)^2
    dist = sqrt(d2)
    dist_scaling = d_norm / dist
    log_sum -= log(dist_scaling)
    return (1 / particle_t(ref_particle)) * log_sum
end

function lyapunov_exponent_precise(pot, y0, T, dt=0.001, d_norm=1e-6)
    ref_particle = Quasi2DIntegrator(pot, y0, d_norm / √2, dt)
    delta_particle = Quasi2DIntegrator(pot, y0 + d_norm / √2, 0, dt)
    return lyapunov_inner(ref_particle, delta_particle, T)
end

function lyapunov_exponent(pot, y0, T, dt=0.001, d_norm=1e-6)
    d_min = d_norm / 10
    d_max = d_norm * 10
    d2_min = d_min^2
    d2_max = d_max^2
    ray_py = [0.0, d_norm / √2]
    ray_y = [y0, y0 + d_norm / √2]
    x = 0.0
    log_sum::Float64 = 0.0
    while x < T
        ray_py .+= dt * force_y.(Ref(pot), x, ray_y)
        ray_y .+= dt .* ray_py
        x += dt
        # Possibly perform scaling
        d2 = (ray_y[1] - ray_y[2])^2 + (ray_py[1] - ray_py[2])^2
        if d2 < d2_min || d2 > d2_max
            dist = sqrt(d2)
            dist_scaling = d_norm / dist
            y2 = ray_y[1] + (ray_y[2] - ray_y[1]) * dist_scaling
            py2 = ray_py[1] + (ray_py[2] - ray_py[1]) * dist_scaling
            ray_y[2] = y2
            ray_py[2] = py2
            log_sum -= log(dist_scaling)
        end
    end
    # Perform last scaling
    dist = abs(ray_y[1] - ray_y[2])
    dist_scaling = d_norm / dist
    log_sum -= log(dist_scaling)
    return (1 / T) * log_sum
end

function quasi2d_map!(du,u,p,t)
    y,py = u
    potential, dt = p
    # kick
    du[2] = py + dt * force_y(potential, dt*t, y)
    # drift
    du[1] = y + dt * du[2]
    return nothing
end

function lyapunov_exponent_ct(pot, y::Float64, T, dt)
    df = DeterministicIteratedMap(quasi2d_map!, [y, 0], [pot, dt])
    return lyapunov(df, ceil(Int64, T/dt)) / dt
end

function exponents(pot, ys, T, dt)
    les = zeros(length(ys))
    Threads.@threads for i ∈ 1:length(ys)
        #les[i] = lyapunov_exponent_ct(pot, ys[i], T, dt)
        les[i] = lyapunov_exponent(pot, ys[i], T, dt)
    end
    return les
end

v0::Float64 = 0.08
T::Float64 = 2000
dt::Float64 = 0.005
H::Float64 = 6
softness = 0.10
dot_radius :: Float64 = 0.25
# angle = 0.0942
angle = deg2rad(2)
pot = LatticePotential(rotation_matrix(angle), dot_radius, v0, softness=softness)
pot_cos = fermi_dot_lattice_cos_series(1, 1.0, 0.25, v0,
    softness=softness)

lyapunov_ys = LinRange(0, H, 200)
@time les = exponents(pot, lyapunov_ys, T, dt)
display(lines(lyapunov_ys, les))

## Plot real and phase spaces
function quasi2d_histogram_intensity(ray_y, xs, ys, potential)
    dt = xs[2] - xs[1]
    dy = ys[2] - ys[1]
    width = length(xs)
    height = length(ys)
    hist = zeros(height, width)
    ray_py = zero(ray_y)
    for (xi, x) ∈ enumerate(xs)
        # kick
        ray_py .+= dt .* force_y.(Ref(potential), x, ray_y)
        # drift
        ray_y .+= dt .* ray_py
        # Collect
        for y ∈ ray_y
            yi = 1 + round(Int, (y - ys[1]) / dy)
            if yi >= 1 && yi <= height
                hist[yi, xi] += 1
            end
        end
    end
    return hist
end

# Separate unstable and stable rays
stable_y::Vector{Float64} = []
unstable_y::Vector{Float64} = []
les_threshold = 0.04
num_rays = 50000
all_ys = LinRange(0, H, num_rays)
is_stable = zeros(Bool, num_rays)
begin
    local ll::Int64 = 1
    for (i, y) ∈ enumerate(all_ys)
        if y > lyapunov_ys[ll+1]
            ll += 1
        end
        if les[ll] > les_threshold || les[ll+1] > les_threshold
            is_stable[i] = false
            append!(unstable_y, y)
        else
            is_stable[i] = true
            append!(stable_y, y)
        end
    end
end

aspect_ratio = 2
res = 512
image_xs = LinRange(0, aspect_ratio*H, aspect_ratio*res)
image_ys = LinRange(0, H, res)

chaos_hist = quasi2d_histogram_intensity(unstable_y, image_xs, image_ys, pot)
super_hist = quasi2d_histogram_intensity(stable_y, image_xs, image_ys, pot)

# axis=(aspect=DataAspect(),))

V = grid_eval(image_xs, image_ys, pot)
minV, maxV = extrema(V)

# pot_colormap = ColorSchemes.Greens
colors_pot= ColorScheme([
    RGB(1, 1, 1),
    RGB(0, 1, 0)
])
colors_super = ColorScheme([
    RGB(1, 1, 1),
    RGB(0, 0, 1)
])
colors_chaos = ColorScheme([
    RGB(1, 1, 1),
    RGB(1, 0, 0)
])
pot_img = get(colors_pot, 0.4 * (V .- minV) ./ (maxV - minV + 1e-9))
maxD =  2.5 * num_rays / res
if false
    maxLD = Makie.pseudolog10(10 * maxD)
    chaos_img = get(colors_chaos, Makie.pseudolog10.(chaos_hist) / maxLD)
    super_img = get(colors_super, Makie.pseudolog10.(super_hist) / maxLD)
else
    chaos_img = get(colors_chaos, (chaos_hist) / maxD)
    super_img = get(colors_super, (super_hist) / maxD)
end

space_img = 
    mapc.((s,c,v) -> clamp(min(s, c, v), 0, 1), super_img, chaos_img, pot_img)
# image(image_xs, image_ys, space_img', axis=(aspect=DataAspect(),))
image(image_xs, image_ys, space_img', axis=(aspect=DataAspect(),))

fig = Figure(resolution=(800,300))

ax_space = Axis(fig[1,1], 
    xlabel=L"x",
    ylabel=L"y",
    aspect=DataAspect())
hidedecorations!(ax_space, label=false)
image!(ax_space, space_img')

# Phase space
#

poincare_num_rays = 30
poincare_start_y = LinRange(0, 1, poincare_num_rays)
poincare_les = exponents(pot, poincare_start_y, T, dt)
poincare_is_stable = poincare_les .< les_threshold
# end_T::Float64 = 12
end_T::Float64 = 1000
ax = Axis(fig[1,2],
    xlabel=L"\tilde{y}",
    ylabel=L"p_y",
    limits=((0,1), (-1,1)),
    aspect=1
)
colsize!(fig.layout, 1, Auto(2))
colsize!(fig.layout, 2, Auto(1))
hidedecorations!(ax, label=false)
# Produce image
points::Vector{SVector{2, Float64}} = []
colors::Vector{RGB} = []
for (ti, start_y) ∈ enumerate(poincare_start_y)
    t = 0.0
    ints = poincare_intersections(pot, start_y, dt, end_T)
    append!(points, map(x->SVector(x[2], x[1]), ints))
    println("$(length(ints)) intersections")
    color = if poincare_is_stable[ti] 
        RGB(0,ti / (poincare_num_rays+1),1)
        # RGB(0,ti / (poincare_num_rays+1),1)
    else 
        RGB(1,0.5*ti / (poincare_num_rays+1),0)
    end
    append!(colors, fill(color, length(ints)))
 #    scatter!(ax, ints[1], color=color, strokewidth=1
 #    , markersize=10)
 #    scatter!(ax, ints[2], color=color, strokewidth=1
 #    , markersize=4)
end
# scatter!(ax, ints, color=color)
scatter!(ax, points, color=colors, markersize=2,
    rasterize=2)

poincare_num_stable = count(poincare_is_stable)
println("Plotted $poincare_num_stable stable and $(poincare_num_rays-poincare_num_stable) unstable trajectories")
display(fig)

for fmt ∈ ["png","pdf"]
    basename = @sprintf("lyapunov_v0_%.2f_r_%.2f_theta_%.3f_softness_%.2f.%s",
            v0, dot_radius, angle, softness,fmt)
    output_path = "outputs/quasi2d/$(basename)"
    save(output_path, fig)
    println("Wrote $output_path")
end